##### 浏览器图片如果不显示，请自行更换“360极速浏览器”，还没找到具体不显示的原因。

***
***




### 鲸哩农业SaaS系统(Version:2.0.1)

> 体验地址， V3 star star ： (请您先体验V2)

> 体验地址， V2 star star ： [点我访问](http://yun.nxptdn.com)



#### 一、平台简介

<img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/%E7%BB%93%E6%9E%84/1.jpg" max-width="800" />

##### 1. 鲸哩农业SaaS系统(Version:2.0.1)是历时两年半开发一整套农业的平台，看了很多的企业、个人依旧在重复性开发但是发现网上没有合适的。于是自己想把公司一套完整农业平台系统开源。

##### 2. 鲸哩农业SaaS系统v2.0.1 是一套从全闭环（设备端 采集端 PC端 APP端）的农业物联网平台，毫无保留给个人及企业免费使用。




#### 二、功能

<img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/%E7%BB%93%E6%9E%84/1.jpg" />

---
***

- 监控管理：支持海康摄像头监控。
- 控制管理：支持控制设备模式设置：定时控制 | 智能控制 | 循环控制。
- 溯源管理：支持产品从开始 进行过程 结束 全链条采集生产溯源报告。
- 仓库管理：支持仓库监控、仓库统计。
- 传感管理：支持传感器采集、报警、报表统计。
- 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
- 岗位管理：配置系统用户所属担任职务。
- 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
- 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。

#### 三、技术栈
   * 服务端
       - 相关技术：Spring 、MyBatis、Spring Security、Jwt、Mysql、Redis、WebSocket、Netty、Mqtt等
       - 开发工具：IDEA    
   * Web端
       - 相关技术：ES6、Bootstrap等 
       - 开发工具：Visual Studio Code    
   * 移动端（Android / Ios / 微信小程序 / H5）
       - 相关技术：目前提供原生Android;uniapp、[uView](https://www.uviewui.com/)、[uChart](https://www.ucharts.cn/)开发中
       - 开发工具：HBuilder
   * 硬件端
       - 相关技术： ESP-IDF、Arduino、FreeRTOS、海康宇视摄像头等
       - 开发工具：Visual Studio Code 和 Arduino

#### 四、代码目录
&nbsp;&nbsp;&nbsp;&nbsp; wisdom_farming_site --------------- 后台接口<br>
&nbsp;&nbsp;&nbsp;&nbsp; html ---------------  PC端前端界面<br>
&nbsp;&nbsp;&nbsp;&nbsp; android --------------- 安卓前端界面[获取App源码-加群]<br>
&nbsp;&nbsp;&nbsp;&nbsp; device_server --------------- 设备采集端[获取App源码-加群] 

#### 五、交流群

微信：thinkaicto（添加微信请先star，备注申请原因）
#### 六、在线体验

- 17615155555/766554  

演示地址：http://yun.nxptdn.com

官方网站：http://wenhui.nxptdn.com/


#### 七、演示图

##### PC端

<img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/PC%E7%AB%AF/1.jpg"/>

***

<img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/PC%E7%AB%AF/2.jpg"/>


##### APP端
<table>
    <tr>
        <td><img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/APP%E7%AB%AF/1.jpg"/></td>
        <td><img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/APP%E7%AB%AF/2.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/APP%E7%AB%AF/3.jpg"/></td>
        <td><img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/APP%E7%AB%AF/4.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/APP%E7%AB%AF/5.jpg"/></td>
        <td><img src="https://wenhui-1251454246.cos.ap-nanjing.myqcloud.com/ny/nongye-v2/APP%E7%AB%AF/6.jpg"/></td>
    </tr>
</table>
